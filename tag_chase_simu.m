####################################################################
# Simulation file for the tag-chase case study available in
# [A. Le Coënt, L. Fribourg, Guaranteed Control of Sampled Switched Systems using Semi-Lagrangian Schemes and One-Sided Lipschitz Constants](https://arxiv.org/pdf/1903.05882.pdf)
####################################################################


####################################################################
### FUNCTIONS AND VARIABLES ########################################
####################################################################

addpath('./functions')

debug_on_error(1)
debug_on_interrupt(1)

load ./controllers/result_tag_chase.mat

T_max = 150; #max time for the simulation
X0 = [1.7;1.7;1.5;1.0]; #initial condition

global dim = 2;
global tau = 0.2;
global numberModes = 6;
global numberModesA = numberModes;
global numberModesB = numberModes;
global maxLength = 1;
global disc_x1 = 10;
global disc_x2 = 10;
global epsilon1;
global epsilon2;
global gamma_prox;
global obj = [1.0;0.0];

x1min = -2;
x1max= 2;
x2min = -2;
x2max = 2;

x1min_safe = -2;
x1max_safe = 2;
x2min_safe = -2;
x2max_safe = 2;

global V1 = 1.1;
global V2 = 1.0;

global theta = linspace(0,2*pi,numberModes+1)'; 
#modes_A = t(1:end-1);
#modes_B = t(1:end-1);


#circsx = delta0.*cos(t) + X0(1); 
#circsy = delta0.*sin(t) + X0(2); 
#plot(circsx,circsy,'color','black','linewidth',2); 


global fT = @(x,T,modeV,modeT) [modeV*sin(theta(modeT));modeV*cos(theta(modeT))];

function A = alpha_c(modeV,modeT)
	A = eye(2);
endfunction 

function B = beta_c(modeV,modeT)
	global tau;
	global theta;
	B = tau*[modeV*sin(theta(modeT));modeV*cos(theta(modeT))];

endfunction 

function point_image = affine_transfo(point,modeV,modeT)
	if modeT ==0
		point_image = point;
	else
		point_image = [alpha_c(modeV,modeT)*point + beta_c(modeV,modeT)];
	endif
endfunction

function zon_image = affine_transfo_zon(zon,modeV,modeT)
	[C,G] = zonotope_decompo(zon);
	
	if modeT ==0
		zon_image = zon;
	else
		zon_image = [alpha_c(modeV,modeT)*C + beta_c(modeV,modeT),alpha_c(modeV,modeT)*G];
	endif
endfunction

function value = cost(state)
	value = sqrt((state(1)-state(3))^2+(state(2)-state(4))^2);
endfunction

function [t,y] = post_y(y0,modeV,modeT)
	global h;
	global fT;
	global tau;
	vopt = odeset ("RelTol", 1e-10, "AbsTol", 1e-10,"NormControl", "on");
		[t,y] = ode45(@(t,x) fT(t,x,modeV,modeT),[0,tau],y0,vopt);
endfunction

####################################################################
### TILING #########################################################
####################################################################


S_safe = [(x1min_safe+x1max_safe)/2,(x1max_safe-x1min_safe)/2,0,0,0;
	  (x2min_safe+x2max_safe)/2,0,(x2max_safe-x2min_safe)/2,0,0;
	  (x1min_safe+x1max_safe)/2,0,0,(x1max_safe-x1min_safe)/2,0;
	  (x2min_safe+x2max_safe)/2,0,0,0,(x2max_safe-x2min_safe)/2];

S_safe1 = [(x1min_safe+x1max_safe)/2,(x1max_safe-x1min_safe)/2,0;
	  (x2min_safe+x2max_safe)/2,0,(x2max_safe-x2min_safe)/2];

S_safe2 = [(x1min_safe+x1max_safe)/2,(x1max_safe-x1min_safe)/2,0;
	  (x2min_safe+x2max_safe)/2,0,(x2max_safe-x2min_safe)/2];


x1 = linspace(x1min_safe,x1max_safe,disc_x1);
x2 = linspace(x2min_safe,x2max_safe,disc_x2);
#epsilon = 1/2*norm([(x1max_safe-x1min_safe)/(disc_x1-1);(x2max_safe-x2min_safe)/(disc_x2-1)]);
epsilon1 = 1/2*abs([(x1max_safe-x1min_safe)/(disc_x1-1)]);
epsilon2 = 1/2*abs([(x2max_safe-x2min_safe)/(disc_x2-1)]);
[X1,X2] = meshgrid(x1,x2);

global rep_x1 = (x1(2:end)+x1(1:end-1))/2;
global rep_x2 = (x2(2:end)+x2(1:end-1))/2;
[rep_X1,rep_X2] = meshgrid(rep_x1,rep_x2);

gamma_prox = 1/2*sqrt(epsilon1^2+epsilon2^2);

#keyboard

#nice one X0 = [1.7;1.7;1.5;1.0];

#X0 = [closest_representative(X0(1:2),rep_x1,rep_x2);closest_representative(X0(3:4),rep_x1,rep_x2)];

y1 = [X0(1:2)];
y2 = [X0(3:4)];
t1 = [0];
t2 = [0];

Y1 = [X0(1:2)];
T1 = [0];
Y2 = [X0(3:4)];
T2 = [0];

#keyboard
i=1;
test_test = 0;
mem1 = [0;0;0;0];
#while (cost([Y1(:,end);Y2(:,end)])>=gamma_prox) && i<=T_max
while  i<=T_max

i
	pattern1 = [];
	pattern2 = [];
#keyboard
	index_pat = [closest_representative_index_2D(y1(:,end),rep_x1,rep_x2);closest_representative_index_2D(y2(:,end),rep_x1,rep_x2)];
	for k = 1:maxLength
    		pattern1 = [pattern1,U(index_pat(1),index_pat(2),index_pat(3),index_pat(4),k+1,1)];
    		pattern2 = [pattern2,U(index_pat(1),index_pat(2),index_pat(3),index_pat(4),k+1,2)];
	endfor

#	next_pat = y(:,end);

#	

#	for k = 1:maxLength

#		index_pat = closest_representative_index(next_pat,rep_x1,rep_x2);
#		next_pat = closest_representative(next_pat,rep_x1,rep_x2);

#    		pattern = [U(index_pat(1),index_pat(2),maxLength-k+2),pattern];
#		next_pat = closest_representative(y(:,end),rep_x1,rep_x2);
#		for kk = 1:k
#			next_pat = closest_representative(affine_transfo(next_pat,pattern(kk)),rep_x1,rep_x2);
#		endfor

#		#index_pat = closest_representative_index(next_pat,rep_x1,rep_x2);
#		
#	endfor

#pattern
#keyboard
    	for j = 1:length(pattern1)
		[tt1,yy1] = post_y(y1(:,end),V1,pattern1(j));
#keyboard
		[tt2,yy2] = post_y(y2(:,end),V2,pattern2(j));
		y1 = [y1,yy1'];
		t1 = [t1,t1(end)+tt1'];
		y2 = [y2,yy2'];
		t2 = [t2,t2(end)+tt2'];

		[YY1] = affine_transfo(Y1(:,end),V1,pattern1(j));
		[YY2] = affine_transfo(Y2(:,end),V2,pattern2(j));
		Y1 = [Y1,YY1];
		Y2 = [Y2,YY2];
		T1 = [T1,T1(end)+tau];
		T2 = [T2,T2(end)+tau];


		for ccc = 1:length(Y1)-1
			if (norm([YY1;YY2]-[Y1(:,ccc);Y2(:,ccc)])<10e-15) && test_test==0
				test_test++
#keyboard
				mem1 = [Y1(:,ccc);Y2(:,ccc)];
break
	#			keyboard
			endif
			if (norm([YY1;YY2]-[Y1(:,ccc);Y2(:,ccc)])<10e-15) && test_test>0 && [Y1(:,ccc);Y2(:,ccc)]!=mem1
				test_test++
break
#keyboard
			endif
			if (norm([YY1;YY2]-[Y1(:,ccc);Y2(:,ccc)])<10e-15) && test_test>0 && [Y1(:,ccc);Y2(:,ccc)]==mem1
#				keyboard
			endif
		endfor	


#keyboard
    	endfor
#keyboard
i++;
endwhile

#plot(t1,y1)
plot(1:length(Y1),Y1)
hold on
#plot(t2,y2)

plot(1:length(Y2),Y2)
leg=legend('i_1','j_1','i_2','j_2');#,'fontsize', 16,'linewidth',2);
set(leg, 'fontsize', 16);
set(leg,'linewidth',2);
set(leg,'location','southeast');
lab1=xlabel("time step");
lab2=ylabel("state value");
set(lab1,'fontsize',16,'linewidth',5);
set(lab2,'fontsize',16,'linewidth',5);
h=get(gcf,'currentaxes');
set(h,'fontsize',12,'linewidth',2)
#plot(T2,Y2)
print -dpdf simu_time_tag_chase.pdf

figure(2)
plot(Y1(1,:),Y1(2,:),'-ob') 
#plot(Y1(1,:),Y1(2,:),'color','blue')
hold on
plot(Y2(1,:),Y2(2,:),'-or') 
plot(X0(1),X0(2),'o','color','black')
plot(X0(3),X0(4),'o','color','black')
plot([-2 -2 2 2 -2],[-2 2 2 -2 -2],'linewidth',2,'color','black')

leg=legend('pursuer','evader');#,'fontsize', 16,'linewidth',2);
set(leg, 'fontsize', 16);
set(leg,'linewidth',2);
set(leg,'location','southeast');
lab1=xlabel("x_1");
lab2=ylabel("x_2");
set(lab1,'fontsize',16,'linewidth',5);
set(lab2,'fontsize',16,'linewidth',5);
h=get(gcf,'currentaxes');
set(h,'fontsize',12,'linewidth',2)
print -dpdf simu_tag_chase.pdf


#plot(y2(1,:),y2(2,:),'color','red')
#plot(obj(1),obj(2),'or')


keyboard

