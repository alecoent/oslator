####################################################################
# Controller synthesis file for the reaction-diffusion case study available in
# [A. Le Coënt, L. Fribourg, Guaranteed optimal reachability control of reaction-diffusion equations using one-sided Lipschitz constants and model reduction](https://hal.archives-ouvertes.fr/hal-02193769/document)
####################################################################


####################################################################
### FUNCTIONS AND VARIABLES ########################################
####################################################################

addpath('./functions')

 tic 




global maxLength = 1;
global disc_x = 15;
global disc_y = 5;
global L = 4;
global delta_x = L/(disc_y-1);
global dim = disc_y-2; #disc_y-2;
global epsilon;


global theta = 0.3;#1/3;
global obj = theta;

global L = 4;
#global delta_x = 1;
global T = 4;
global tau = 0.1;
global sub_step = 1;

global sigma = 0.5;

#global uu = [0,0,0,0.5,0.5,0.5,1,1,1;0,0.5,1,0,0.5,1,0,0.5,1];
global uu = [0,0.2,0.4,0.6,0.8,1.0;0,0.2,0.4,0.6,0.8,1.0];
global numberModes = length(uu);

xmin = 0.0;
xmax= 1.0;


xmin_safe = -0.1;
xmax_safe = 1.1;

global ff = @(y) y.*(1-y).*(y-theta);

function y_dt = f_dt(y,mode)
	global delta_x
	global disc_y
	global uu
	global ff
	global sigma

	y = [uu(1,mode);y(2:end-1);uu(2,mode)];

	y_dt = zeros(disc_y,1);

	y_dt(1) = 0;#uu(1,mode);
	y_dt(disc_y) = 0;#uu(2,mode);
	y_dt(2:(disc_y-1)) = sigma/(delta_x^2)*(y(3:disc_y)-2*y(2:(disc_y-1))+y(1:disc_y-2)) + ff(y(2:(disc_y-1)));
endfunction


function y_dt = f_dt_inside(y,mode)
	global delta_x
	global disc_y
	global uu
	global ff
	global sigma

	y = [uu(1,mode);y;uu(2,mode)];

	y_dt = zeros(disc_y,1);

	y_dt(1) = 0;#uu(1,mode);
	y_dt(disc_y) = 0;#uu(2,mode);
	y_dt(2:(disc_y-1)) = sigma/(delta_x^2)*(y(3:disc_y)-2*y(2:(disc_y-1))+y(1:disc_y-2)) + ff(y(2:(disc_y-1)));
endfunction

global fT = @(t,T,mode) [f_dt(T,mode)];
global fT_inside = @(t,T,mode) [f_dt_inside(T,mode)];#(2:end-1);


##################################################
## OSL CONSTANTS COMPUTATION #####################
##################################################


## COMPUTATION OF max(abs(f(x)))

#x0 = 0.5*ones(dim,1);
#x_low = xmin;
#x_hig = xmax;

#phi = @(i,x) -norm(fT_inside(0,x,i));

#for i = 1:numberModes
#  func = @(x) phi(i,x);
#  [x_opt,obj,info,iter,nf,lambda] = sqp(x0,func,[],[], x_low*ones(dim,1),x_hig*ones(dim,1));
#  MAXF(i) = -func(x_opt);
#endfor


## COMPUTATION OF OSL

#phi2 = @(i,x) -(fT_inside(0,x(1:dim),i)-fT_inside(0,x((dim+1):2*dim),i))'*(x(1:dim)-x((dim+1):2*dim))/norm(x(1:dim)-x((dim+1):2*dim))^2;
#x0 = rand(2*dim,1);

#for i = 1:numberModes
#  [x_opt2,obj2,info2,iter2,nf2,lambda2] = sqp(x0,@(x) phi2(i,x),[],[], x_low*ones(2*dim,1), x_hig*ones(2*dim,1));
#  OSL(i) = -phi2(i,x_opt2);
#endfor


## COMPUTATION OF lipschitz constant

#phi3 = @(i,x) -norm(fT_inside(0,x(1:dim),i)-fT_inside(0,x((dim+1):2*dim),i))^2/norm(x(1:dim)-x((dim+1):2*dim))^2;
#x0 = rand(2*dim,1);

#for i = 1:numberModes
#  func3 = @(x) phi3(i,x);
#  [x_opt3,obj3,info3,iter3,nf3,lambda3] = sqp(x0,func3,[],[], x_low*ones(2*dim,1), x_hig*ones(2*dim,1));
#  LIP(i) = sqrt(-func3(x_opt3));
#endfor


#global beta;
#beta = -OSL;
#global C0f;
#for i = 1:numberModes
#  C0f(i) = LIP(i)*MAXF(i);
#endfor

#keyboard

### fonction delta = delta(i,delta0), le cas OSL=0 n'est pas pris en compte

##function delta = delta_mode(mode,delta_init)
##	 global beta;
##	 global C0f;
##	 global tau;
##	 global sub_step;
##	 deltaa = 0;

##	tau = tau/sub_step;
##	 delta = delta_init;
##	 
##	 if beta(mode) < 0

##	    C0ff = C0f(mode);
##	    betaa = beta(mode);
##	    toto = delta;
##	    NN = 1/1;		
##    	    toto = toto^2*exp(-(2+NN)*betaa*tau) + C0ff^2/((NN*(2+NN))*betaa^2)*(-tau^2 + 2*tau/((2+NN)*betaa) + 2/((2+NN)^2*betaa^2)*(exp(-(2+NN)*betaa*tau)-1));	
##	 elseif beta(mode>=0)
##	    C0ff = C0f(mode);
##	    betaa = beta(mode);	
##	    deltaa = (delta^2*exp(-betaa*tau) + (C0ff/betaa)^2*(tau^2-2*tau/betaa+2/(betaa^2)-2*exp(-betaa*tau)/(betaa^2)))  ;  
##	 endif
##    	deltaa = sqrt(deltaa);
##	tau = tau * sub_step;
##    	delta = deltaa;
##endfunction


function A = alpha_c(mode,zonotope)
	global dim
	A = eye(dim);	
endfunction 

function B = beta_c(mode,zonotope)
	global tau
	global fT_inside
	DF = fT_inside(0,zonotope(:,1),mode);
	B = tau*DF(2:end-1);
endfunction 


function value = cost(state,mode)
#	global theta
	global disc_y
	global obj
	global uu
	value = norm([uu(1,mode);state;uu(2,mode)]-obj*ones(disc_y,1));
#	value = norm(state-ones(dim,1));
#	value = norm(state-0*ones(dim,1));
endfunction

function [t,y] = post_y(y0,mode)
	global h
	global fT
	global tau
	global uu

	y0 = [uu(1,mode);y0(2:end-1);uu(2,mode)];
	vopt = odeset ("RelTol", 1e-10, "AbsTol", 1e-10,"NormControl", "on");
		[t,y] = ode45(@(t,x) fT(t,x,mode),[0,tau],y0,vopt);
#		y(:,1) = ones(1,length(t))*uu(1,mode);
#		y(:,end) = ones(1,length(t))*uu(2,mode);

endfunction

#pkg load specfun

#AA = OSL(1)*0.033^2-2*C0f(1)^2/OSL(1)^3;
#BB = 2*C0f(1)^2/OSL(1)^2
#DD = 2*C0f(1)^2/OSL(1)^3
#res = (-BB*lambertw(AA*OSL(1)*exp(-OSL(1)*DD/BB)/BB)-OSL(1)*DD)/(OSL(1)*BB)

#GG1 = sqrt(3)*0.033*abs(OSL(1))/C0f(1)
#GG2 = sqrt(3)*0.033*abs(OSL(1))/C0f(3)

#alpha_u1 = abs(OSL(1))*GG1/4
#alpha_u2 = abs(OSL(1))*GG2/4

#time_u1 = GG1*(1-alpha_u1)
#time_u2 = GG1*(1-alpha_u2)
#keyboard

####################################################################
### TILING AND SYNTHESIS ###########################################
####################################################################

#dim = 2
S_safe = [ones(dim,1)*(xmin_safe+xmax_safe)/2,eye(dim)*(xmax_safe-xmin_safe)/2];


#x = zeros(dim,disc_x);
x = linspace(xmin_safe,xmax_safe,disc_x);


#epsilon = 1/2*norm([(x1max_safe-x1min_safe)/(disc_x1-1);(x2max_safe-x2min_safe)/(disc_x2-1)]);
epsilon = 1/2*norm([(xmax_safe-xmin_safe)/(disc_x-1)]);

#[X1,X2] = meshgrid(x1,x2);

rep_x = (x(2:end)+x(1:end-1))/2;
#rep_x2 = (x2(2:end)+x2(1:end-1))/2;
[rep_X1,rep_X2] = meshgrid(rep_x,rep_x);

V = zeros([size(ones(repmat(disc_x-1,1,dim))),maxLength+1]);
U = zeros([size(ones(repmat(disc_x-1,1,dim))),maxLength+1]);

nn = dim;
xx = (0:(disc_x-1)^dim-1)';
MM = zeros((disc_x-1)^dim,dim);

for k = 1:nn
	MM(:,k) = mod(xx,disc_x-1);
	xx = (xx-MM(:,k))/(disc_x-1);
endfor

multi_index = (MM+1)';

#keyboard
X = 0.5*ones(dim,1);
rep = closest_representative(X,rep_x);
X = rep;

mem = 1000;
mem_index = 0;

#for k1 = 1:disc_x1-1
#	for k2 = 1:disc_x2-1

#keyboard

for k = multi_index
#keyboard
		X0 = [rep_x(k)]';
		X = X0;
		zon = [X(:),epsilon*eye(dim)];

		for i = 1:maxLength

			mem = 1000;
			#mem_index = 0;
			mem_next = X0;

#i
#keyboard
			for j = 1:numberModes

				flag = 0;


				next_X = affine_transfo(X,j);
				next_zon = affine_transfo_zon(zon,j);
#keyboard

				if inclusion_zonotope(next_zon,S_safe) 
#&& is_in_zonotope(next_X,S_safe)

					next = closest_representative(next_X,rep_x);
					next_next_zon = [next,epsilon*eye(dim)]; #[next(1),epsilon1,0;next(2),0,epsilon2];

					for k3 = 1:i
#keyboard

						#toto = rand(9,9);
#						idx.type ="()";
#						idx.subs = {":",k3};
#						U_temp = subsref(U,idx);


						U_temp = U(:);
						index_temp0 = 1;
						for k4 = 1:dim
							index_temp0 = index_temp0+((disc_x-1)^(k4-1))*(k'(k4)-1);
#keyboard
						endfor
						index_temp = index_temp0 + (disc_x-1)^(dim)*(k3-1);
						mode_temp = U_temp(index_temp);
#						mode_temp = U_temp(
#keyboard
#						U_temp = U_temp(1:disc_x,(((k3-1)*disc_x)+1):k3*disc_x);
#						mode_temp = U_temp(

#						next_X = affine_transfo(next,U([k',k3]));
						next_X = affine_transfo(next,mode_temp);
#keyboard

#						next_zon = affine_transfo_zon(next_next_zon,U([k',k3]));
						next_zon = affine_transfo_zon(next_next_zon,mode_temp);
#keyboard
						if inclusion_zonotope(next_zon,S_safe) #is_in_zonotope(next_X,S_safe)
						next = closest_representative(next_X,rep_x);
						next_next_zon = [next,epsilon*eye(dim)];
						else
							flag = 1;
							break
						endif
					endfor

					if flag == 1
						break
					endif

					value = cost(next,j);

					if mem>=value
						mem = value;
						#mem_index = j;
						mem_next = next;

						U(index_temp0+(disc_x-1)^(dim)*(i)) = j;
						V(index_temp0+(disc_x-1)^(dim)*(i)) = mem;
						

					endif
#				else 	
#					value = cost(next_X);
#					mem = 1000;
#					#mem_index = 0;
#					U(k1,k2,i+1) = 0;
#					V(k1,k2,i+1) = mem;

				endif
			endfor

			#X = mem_next;

		#	[next_X,arg_min] = min(cost(affine_transfo(X,1:4)),[],2)


		endfor
#	endfor
endfor



####################################################################
### SIMULATION #####################################################
####################################################################


toc

save result_reaction.mat U


keyboard



