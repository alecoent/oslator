####################################################################
# Simulation file for the (high dimension) reaction-diffusion case study with model reduction available in
# [A. Le Coënt, L. Fribourg, Guaranteed optimal reachability control of reaction-diffusion equations using one-sided Lipschitz constants and model reduction](https://hal.archives-ouvertes.fr/hal-02193769/document)
####################################################################


####################################################################
### FUNCTIONS AND VARIABLES ########################################
####################################################################

addpath('./functions')

 tic 




global maxLength = 1;
global disc_x = 15;
global disc_y = 5;
global disc_y_HD = 10;#2*disc_y;
global L = 4;
global delta_x = L/(disc_y-1);
global delta_x_HD = L/(disc_y_HD-1);
global dim = disc_y-2;#disc_y-2;
global dim_HD = disc_y_HD-2;#disc_y-2;
global epsilon;
#global obj = [1.0;0.0];

T_max = 20; #max time for the simulation
X00 = 0:L/(disc_y_HD-1):L
X0 = 0.8.*X00/L + 0.1.*(1-X00./L) #initial condition
X0 = X0'
Y0 = X0(2:end-1)


global obj = 0;

global theta = 0.3;#1/3;
global L = 4;
#global delta_x = 1;
global T = 4;
global tau = 0.1;
tau = tau/100;

global sigma = 1.0;

global C = zeros(disc_y,disc_y_HD);

for i = 1:disc_y
	C(i,2*i) = 1/sqrt(2);
	C(i,2*i-1) = 1/sqrt(2);
endfor

#for i = 1:disc_y
#	C(i,4*i) = 1/sqrt(4);
#	C(i,4*i-1) = 1/sqrt(4);
#	C(i,4*i-2) = 1/sqrt(4);
#	C(i,4*i-3) = 1/sqrt(4);
#endfor

#global uu = [0,0,0,0.5,0.5,0.5,1,1,1;0,0.5,1,0,0.5,1,0,0.5,1];

global uu = [0,0.2,0.4,0.6,0.8,1.0;0,0.2,0.4,0.6,0.8,1.0];

global numberModes = length(uu);

xmin = 0.0;
xmax= 1.0;


xmin_safe = 0.0;
xmax_safe = 1.0;

global ff = @(y) y.*(1-y).*(y-theta);

#function y_next = f_next(y,mode)
#	global delta_x
#	global tau
#	global disc_y
#	global uu
#	global ff

#	y = [uu(1,mode);y(2:end-1);uu(2,mode)];

#	y_next = zeros(disc_y,1);

#	y_next(1) = uu(1,mode);
#	y_next(disc_y) = uu(2,mode);
#	y_next(2:(disc_y-1)) = y(2:(disc_y-1)) + tau/(delta_x^2)*(y(3:disc_y)-2*y(2:(disc_y-1))+y(1:disc_y-2)) + tau*ff(y(2:(disc_y-1)));
#endfunction

function y_dt = f_dt(y,mode)
	global delta_x_HD
	global disc_y_HD
	global uu
	global ff
	global sigma

	y = [uu(1,mode);y(2:end-1);uu(2,mode)];

	y_dt = zeros(disc_y_HD,1);

	y_dt(1) = 0;#uu(1,mode);
	y_dt(disc_y_HD) = 0;#uu(2,mode);
	y_dt(2:(disc_y_HD-1)) = sigma/(delta_x_HD^2)*(y(3:disc_y_HD)-2*y(2:(disc_y_HD-1))+y(1:disc_y_HD-2)) + ff(y(2:(disc_y_HD-1)));
endfunction


function y_dt = f_dt_inside(y,mode)
	global delta_x_HD
	global disc_y_HD
	global uu
	global ff
	global sigma

	y = [uu(1,mode);y;uu(2,mode)];

	y_dt = zeros(disc_y_HD,1);

	y_dt(1) = 0;#uu(1,mode);
	y_dt(disc_y_HD) = 0;#uu(2,mode);
	y_dt(2:(disc_y_HD-1)) = sigma/(delta_x_HD^2)*(y(3:disc_y_HD)-2*y(2:(disc_y_HD-1))+y(1:disc_y_HD-2)) + ff(y(2:(disc_y_HD-1)));
endfunction

global fT = @(t,T,mode) [f_dt(T,mode)];
global fT_inside = @(t,T,mode) [f_dt_inside(T,mode)];

function A = alpha_c(mode,zonotope)
	global dim_HD
	A = eye(dim_HD);	
endfunction 

function B = beta_c(mode,zonotope)
	global tau
	global fT_inside
	DF = fT_inside(0,zonotope(:,1),mode);
	B = tau*DF(2:end-1);
endfunction 


function value = cost(state)
	global theta
	value = norm(state-theta);
endfunction

function [t,y] = post_y(y0,mode)
	global h
	global fT
	global tau
	global uu

	y0 = [uu(1,mode);y0(2:end-1);uu(2,mode)];
	vopt = odeset ("RelTol", 1e-10, "AbsTol", 1e-10,"NormControl", "on");
		[t,y] = ode45(@(t,x) fT(t,x,mode),[0,tau],y0,vopt);
#		y(:,1) = ones(1,length(t))*uu(1,mode);
#		y(:,end) = ones(1,length(t))*uu(2,mode);

endfunction

####################################################################
### TILING #########################################################
####################################################################

#dim = 2
S_safe = [ones(dim,1)*(xmin_safe+xmax_safe)/2,eye(dim)*(xmax_safe-xmin_safe)/2];


#x = zeros(dim,disc_x);
x = linspace(xmin_safe,xmax_safe,disc_x);


#epsilon = 1/2*norm([(x1max_safe-x1min_safe)/(disc_x1-1);(x2max_safe-x2min_safe)/(disc_x2-1)]);
epsilon = 1/2*norm([(xmax_safe-xmin_safe)/(disc_x-1)]);

#[X1,X2] = meshgrid(x1,x2);

rep_x = (x(2:end)+x(1:end-1))/2;
#rep_x2 = (x2(2:end)+x2(1:end-1))/2;
[rep_X1,rep_X2] = meshgrid(rep_x,rep_x);

V = zeros([size(ones(repmat(disc_x-1,1,dim))),maxLength+1]);
U = zeros([size(ones(repmat(disc_x-1,1,dim))),maxLength+1]);

#multi_index = 

#for i = 1:dim
##	for j = 1:disc_x-1
#		multi_index(i,1:i:(disc_x-1)^dim) = repmat([1:disc_x-1],1,(disc_x-1)^(dim-i))
##	endfor
#endfor


#multi_index(1,:) = repmat(1:disc_x-1,1,disc_x-1)
#multi_index(2,:) = 

#for i = 1:disc_x1-1
#	for j = 1:disc_x2-1
#		V(i,j,1) = cost([rep_x1(i);rep_x2(j)]);
#	endfor
#endfor

nn = dim;
xx = (0:(disc_x-1)^dim-1)';
MM = zeros((disc_x-1)^dim,dim);

for k = 1:nn
	MM(:,k) = mod(xx,disc_x-1);
	xx = (xx-MM(:,k))/(disc_x-1);
endfor

multi_index = (MM+1)';

#for i = multi_index
#	i
#i(1)
#i(2)
#i(3)
#endfor





#keyboard
X = 0.5*ones(dim,1);
rep = closest_representative(X,rep_x);
X = rep;

mem = 1000;
mem_index = 0;

#for k1 = 1:disc_x1-1
#	for k2 = 1:disc_x2-1

#keyboard

#for k = multi_index
##keyboard
#		X0 = [rep_x(k)]';
#		X = X0;
#		zon = [X(:),epsilon*eye(dim)];

#		for i = 1:maxLength

#			mem = 1000;
#			#mem_index = 0;
#			mem_next = X0;

##i
##keyboard
#			for j = 1:numberModes

#				flag = 0;


#				next_X = affine_transfo(X,j);
#				next_zon = affine_transfo_zon(zon,j);
##keyboard

#				if inclusion_zonotope(next_zon,S_safe) 
##&& is_in_zonotope(next_X,S_safe)

#					next = closest_representative(next_X,rep_x);
#					next_next_zon = [next,epsilon*eye(dim)]; #[next(1),epsilon1,0;next(2),0,epsilon2];

#					for k3 = 1:i
##keyboard

#						#toto = rand(9,9);
##						idx.type ="()";
##						idx.subs = {":",k3};
##						U_temp = subsref(U,idx);


#						U_temp = U(:);
#						index_temp0 = 1;
#						for k4 = 1:dim
#							index_temp0 = index_temp0+((disc_x-1)^(k4-1))*(k'(k4)-1);
##keyboard
#						endfor
#						index_temp = index_temp0 + (disc_x-1)^(dim)*(k3-1);
#						mode_temp = U_temp(index_temp);
##						mode_temp = U_temp(
##keyboard
##						U_temp = U_temp(1:disc_x,(((k3-1)*disc_x)+1):k3*disc_x);
##						mode_temp = U_temp(

##						next_X = affine_transfo(next,U([k',k3]));
#						next_X = affine_transfo(next,mode_temp);
##keyboard

##						next_zon = affine_transfo_zon(next_next_zon,U([k',k3]));
#						next_zon = affine_transfo_zon(next_next_zon,mode_temp);
##keyboard
#						if inclusion_zonotope(next_zon,S_safe) #is_in_zonotope(next_X,S_safe)
#						next = closest_representative(next_X,rep_x);
#						next_next_zon = [next,epsilon*eye(dim)];
#						else
#							flag = 1;
#							break
#						endif
#					endfor

#					if flag == 1
#						break
#					endif

#					value = cost(next);

#					if mem>=value
#						mem = value;
#						#mem_index = j;
#						mem_next = next;

#						U(index_temp0+(disc_x-1)^(dim)*(i)) = j;
#						V(index_temp0+(disc_x-1)^(dim)*(i)) = mem;
#						

#					endif
##				else 	
##					value = cost(next_X);
##					mem = 1000;
##					#mem_index = 0;
##					U(k1,k2,i+1) = 0;
##					V(k1,k2,i+1) = mem;

#				endif
#			endfor

#			#X = mem_next;

#		#	[next_X,arg_min] = min(cost(affine_transfo(X,1:4)),[],2)


#		endfor
##	endfor
#endfor



#####################################################################
#### SIMULATION #####################################################
#####################################################################


toc

#save result_EDP.mat U

#load result_EDP.mat

#keyboard





y = [X0];
t = [0];
m = [0];

Y = [Y0];
T = [0];
M = [0];

load pattern.mat


for i = 1:T_max
i

	pattern = M(i+1);
#	pattern = [];

#	y_red = C*y(:,end);
#	index_pat = closest_representative_index(y_red(2:end-1),rep_x);
#	for k = 2:maxLength+1

#		U_temp = U(:);
#		index_temp0 = 1;
#		for k4 = 1:dim
##keyboard
#			index_temp0 = index_temp0+((disc_x-1)^(k4-1))*(index_pat(k4)-1);
##keyboard
#		endfor
#		index_temp = index_temp0 + (disc_x-1)^(dim)*(k-1);
#		mode_temp = U_temp(index_temp);

#    		pattern = [pattern,mode_temp];

#	endfor


#	next_pat = y(:,end);

#	

#	for k = 1:maxLength

#		index_pat = closest_representative_index(next_pat,rep_x1,rep_x2);
#		next_pat = closest_representative(next_pat,rep_x1,rep_x2);

#    		pattern = [U(index_pat(1),index_pat(2),maxLength-k+2),pattern];
#		next_pat = closest_representative(y(:,end),rep_x1,rep_x2);
#		for kk = 1:k
#			next_pat = closest_representative(affine_transfo(next_pat,pattern(kk)),rep_x1,rep_x2);
#		endfor

#		#index_pat = closest_representative_index(next_pat,rep_x1,rep_x2);
#		
#	endfor

pattern

    	for j = 1:length(pattern)
#keyboard

#		[tt,yy] = post_y(y(:,end),pattern(j));

#		y = [y,yy'];
#		t = [t,t(end)+tt'];
#		m = [m,pattern(j)*ones(1,length(tt'))];
		YY = Y(:,end);
		for ll = 1:100
			[YY] = affine_transfo(YY,pattern(j));
		endfor

		Y = [Y,YY];
		T = [T,T(end)+tau];
#		M = [M,pattern(j)];

#keyboard
    	endfor
#keyboard
endfor

#plot(t,y)
#hold on
#plot(T,Y)

#keyboard
#for i = 1:T_max
	plot_Y = [[0.1,uu(1,M(2:T_max+1))];Y;[0.8,uu(2,M(2:T_max+1))]];
#endfor
#keyboard


plot_X = 0:L/(disc_y_HD-1):L
plot_T = (0:length(plot_Y))*0.1
plot_YY = plot_Y(:,1)'


figure(2)
plot3(zeros(size(plot_YY)),plot_X,plot_YY','color','black')
hold on
for i = 1:length(plot_Y)
	plot_YY = plot_Y(:,i);
 	plot3(ones(size(plot_YY))*plot_T(i),plot_X,plot_YY','color','black')
endfor

lab1=xlabel("t");
lab2=ylabel("x");
lab3=zlabel("y");
set(lab1,'fontsize',16,'linewidth',5);
set(lab2,'fontsize',16,'linewidth',5);
h=get(gcf,'currentaxes');
set(h,'fontsize',12,'linewidth',1)


err = 0;

for i = 1:disc_y_HD-1
	err = err + ((plot_YY(i)-0.3)^2+(plot_YY(i+1)-0.3)^2)/2*delta_x_HD;
endfor

err_euc = norm(plot_YY-theta*ones(disc_y_HD,1))


err_red = norm(C*plot_YY - theta*ones(disc_y,1))

keyboard 

figure(3)
plot(plot_X,plot_Y,'color','black')
hold on
#plot(obj(1),obj(2),'or')
#plot([-2 -2 3 3 -2],[-1 2 2 -1 -1],'linewidth',2,'color','black')
#plot([-1.5 -1.5 2.5 2.5 -1.5],[-0.5 1.5 1.5 -0.5 -0.5],'linewidth',2,'color','blue')
#leg=legend('x_1','x_2','fontsize', 16,'linewidth',2);
#set(leg, 'fontsize', 16);
#et(leg,'linewidth',2);
#set(leg,'location','southeast');
lab1=xlabel("x");
lab2=ylabel("y");
set(lab1,'fontsize',16,'linewidth',5);
set(lab2,'fontsize',16,'linewidth',5);
h=get(gcf,'currentaxes');
set(h,'fontsize',12,'linewidth',2)
print -dpdf simu_two_tank.pdf



keyboard


C = 1/sqrt(2)*[1,1,0,0,0,0;0,0,1,1,0,0;0,0,0,0,1,1]


A = [2,1,0,0,0,0;1,2,1,0,0,0;0,1,2,1,0,0;0,0,1,2,1,0;0,0,0,1,2,1;0,0,0,0,1,2];

keyboard
