####################################################################
# Controller synthesis file for the two-tank case study available in
# [A. Le Coënt, L. Fribourg, Guaranteed Control of Sampled Switched Systems using Semi-Lagrangian Schemes and One-Sided Lipschitz Constants](https://arxiv.org/pdf/1903.05882.pdf)
####################################################################


####################################################################
### FUNCTIONS AND VARIABLES ########################################
####################################################################

addpath('./functions')

 tic 

global dim = 2;
global tau = 0.1;
global numberModes = 4;
global maxLength = 2;
global disc_x1 = 10;
global disc_x2 = 10;
global epsilon1;
global epsilon2;
global obj = [1.0;0.0];

x1min = -1.5;
x1max= 2.5;
x2min = -0.5;
x2max = 1.5;

x1min_safe = -2;
x1max_safe = 3;
x2min_safe = -1;
x2max_safe = 2;

global A_1 = [-1,0;1,0] ;
global B_1 = [-2;0];

global A_2 = [-1,0;1,-1] ;
global B_2 = [-2;-5];

global A_3 = [-1,0;1,0] ;
global B_3 = [3;0];

global A_4 = [-1,0;1,-1] ;
global B_4 = [3;-5];

global A = zeros(dim,dim,numberModes);
A(:,:,1) = A_1;
A(:,:,2) = A_2;
A(:,:,3) = A_3;
A(:,:,4) = A_4;

global B = zeros(2,numberModes);
B(:,1) = B_1;
B(:,2) = B_2;
B(:,3) = B_3;
B(:,4) = B_4;

global fT = @(t,T,mode) [A(1,1,mode)*T(1)+A(1,2,mode)*T(2);
	      A(2,1,mode)*T(1)+A(2,2,mode)*T(2)]+B(:,mode);

function A = alpha_c(mode)
	global tau;
	if (mode == 1)
		A = [exp(-tau), 0; -exp(-tau)+1,1];
	elseif (mode == 3)
		A = [exp(-tau), 0; -exp(-tau)+1,1];
	elseif (mode == 2)
		A = expm([-1,0;1,-1]*tau);
	elseif (mode == 4)
		A = expm([-1,0;1,-1]*tau);
	endif
endfunction 

function B = beta_c(mode)
	global tau;
	if (mode == 1)
		B = 2 * [exp(-tau) - 1;1-exp(-tau)-tau];
	elseif (mode == 3)
		B = -3 * [exp(-tau) - 1;1-exp(-tau)-tau];
	elseif (mode == 2)
		B = (expm([-1,0;1,-1]*tau) - eye(2))*inv([-1,0;1,-1])*[-2;-5];
	elseif (mode == 4)
		B = (expm([-1,0;1,-1]*tau) - eye(2))*inv([-1,0;1,-1])*[3;-5];
	endif
	
endfunction 

function point_image = affine_transfo(point,mode)
	if mode ==0
		point_image = point;
	else
		point_image = [alpha_c(mode)*point + beta_c(mode)];
	endif
endfunction

function zon_image = affine_transfo_zon(zon,mode)
	[C,G] = zonotope_decompo(zon);
	
	if mode ==0
		zon_image = zon;
	else
		zon_image = [alpha_c(mode)*C + beta_c(mode),alpha_c(mode)*G];
	endif
endfunction

function value = cost(state)
	global obj
	value = norm(state-obj);
endfunction

function [t,y] = post_y(y0,mode)
	global h;
	global fT;
	global tau;
	vopt = odeset ("RelTol", 1e-10, "AbsTol", 1e-10,"NormControl", "on");
		[t,y] = ode45(@(t,x) fT(t,x,mode),[0,tau],y0,vopt);
endfunction

####################################################################
### TILING #########################################################
####################################################################


S_safe = [(x1min_safe+x1max_safe)/2,(x1max_safe-x1min_safe)/2,0;
	  (x2min_safe+x2max_safe)/2,0,(x2max_safe-x2min_safe)/2];

x1 = linspace(x1min_safe,x1max_safe,disc_x1);
x2 = linspace(x2min_safe,x2max_safe,disc_x2);
#epsilon = 1/2*norm([(x1max_safe-x1min_safe)/(disc_x1-1);(x2max_safe-x2min_safe)/(disc_x2-1)]);
epsilon1 = 1/2*norm([(x1max_safe-x1min_safe)/(disc_x1-1)]);
epsilon2 = 1/2*norm([(x2max_safe-x2min_safe)/(disc_x2-1)]);
[X1,X2] = meshgrid(x1,x2);

rep_x1 = (x1(2:end)+x1(1:end-1))/2;
rep_x2 = (x2(2:end)+x2(1:end-1))/2;
[rep_X1,rep_X2] = meshgrid(rep_x1,rep_x2);

V = zeros([size(rep_X1),maxLength+1]);
U = zeros([size(rep_X1),maxLength+1]);

for i = 1:disc_x1-1
	for j = 1:disc_x2-1
		V(i,j,1) = cost([rep_x1(i);rep_x2(j)]);
	endfor
endfor

X = [1.3;0.2];
rep = closest_representative_2D(X,rep_x1,rep_x2);
X = rep;

mem = 1000;
mem_index = 0;

for k1 = 1:disc_x1-1
	for k2 = 1:disc_x2-1

		X0 = [rep_x1(k1);rep_x2(k2)];
		X = X0;
		zon = [X(1),epsilon1,0;X(2),0,epsilon2];

		for i = 1:maxLength

			mem = 1000;
			#mem_index = 0;
			mem_next = X0;

#i
#keyboard
			for j = 1:numberModes

				flag = 0;


				next_X = affine_transfo(X,j);
				next_zon = affine_transfo_zon(zon,j);
#keyboard

				if inclusion_zonotope(next_zon,S_safe) 
#&& is_in_zonotope(next_X,S_safe)

					next = closest_representative_2D(next_X,rep_x1,rep_x2);
					next_next_zon = [next(1),epsilon1,0;next(2),0,epsilon2];

					for k3 = 1:i
						next_X = affine_transfo(next,U(k1,k2,k3));
						next_zon = affine_transfo_zon(next_next_zon,U(k1,k2,k3));
#keyboard
						if inclusion_zonotope(next_zon,S_safe) #is_in_zonotope(next_X,S_safe)
						next = closest_representative_2D(next_X,rep_x1,rep_x2);
						next_next_zon = [next(1),epsilon1,0;next(2),0,epsilon2];
						else
							flag = 1;
							break
						endif
					endfor

					if flag == 1
						break
					endif

					value = cost(next);

					if mem>=value
						mem = value;
						#mem_index = j;
						mem_next = next;
						U(k1,k2,i+1) = j;
						V(k1,k2,i+1) = mem;
					endif
#				else 	
#					value = cost(next_X);
#					mem = 1000;
#					#mem_index = 0;
#					U(k1,k2,i+1) = 0;
#					V(k1,k2,i+1) = mem;

				endif
			endfor

			#X = mem_next;

		#	[next_X,arg_min] = min(cost(affine_transfo(X,1:4)),[],2)


		endfor
	endfor
endfor

save result_two_tank.mat U

####################################################################
### SIMULATION #####################################################
####################################################################


toc


keyboard

T_max = 30;
X0 = [2.5;1.5];

y = [X0];
t = [0];

Y = [X0];
T = [0];

for i = 1:T_max
i
	pattern = [];

	index_pat = closest_representative_index_2D(y(:,end),rep_x1,rep_x2);
	for k = 1:maxLength
    		pattern = [pattern,U(index_pat(1),index_pat(2),k+1)];
	endfor


pattern

    	for j = 1:length(pattern)
		[tt,yy] = post_y(y(:,end),pattern(j));
		y = [y,yy'];
		t = [t,t(end)+tt'];

		[YY] = affine_transfo(Y(:,end),pattern(j));
		Y = [Y,YY];
		T = [T,T(end)+tau];

#keyboard
    	endfor
#keyboard
endfor

#plot(t,y)
#hold on
#plot(T,Y)

figure(2)
plot(y(1,:),y(2,:),'color','black')
hold on
plot(obj(1),obj(2),'or')
plot([-2 -2 3 3 -2],[-1 2 2 -1 -1],'linewidth',2,'color','black')
plot([-1.5 -1.5 2.5 2.5 -1.5],[-0.5 1.5 1.5 -0.5 -0.5],'linewidth',2,'color','blue')
#leg=legend('x_1','x_2','fontsize', 16,'linewidth',2);
#set(leg, 'fontsize', 16);
#et(leg,'linewidth',2);
#set(leg,'location','southeast');
lab1=xlabel("x_1");
lab2=ylabel("x_2");
set(lab1,'fontsize',16,'linewidth',5);
set(lab2,'fontsize',16,'linewidth',5);
h=get(gcf,'currentaxes');
set(h,'fontsize',12,'linewidth',2)
print -dpdf simu_two_tank.pdf



keyboard

