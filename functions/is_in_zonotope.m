function answer = is_in_zonotope(point,zonotope)
	zonotope_point = [point,zeros(length(point),length(point))];
	answer = inclusion_zonotope(zonotope_point,zonotope);
endfunction
