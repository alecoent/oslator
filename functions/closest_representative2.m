function rep = closest_representative2(X,x1,x2)


	global epsilon1
	global epsilon2
#	rep1 = x1(abs(x1(:)-X(1))<=epsilon/sqrt(2));
#	rep2 = x2(abs(x2(:)-X(2))<=epsilon/sqrt(2));
	rep1 = x1(abs(x1(:)-X(1))<=epsilon1);
	rep2 = x2(abs(x2(:)-X(2))<=epsilon2);
#if size(rep2) == [1 0]
#keyboard
#endif
	rep = [rep1(1);rep2(1)];
endfunction
