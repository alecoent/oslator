function rep = closest_representative_index(X,x)
	global epsilon
	global dim

	flags = ones(dim,1);
	rep = zeros(dim,1);

	for kd = 1:dim
#keyboard
		i=1;
		while(flags(kd) && i<=length(x))
			if abs(x(i)-X(kd))<epsilon
				rep(kd) = i;
				flags(kd)=0;
			endif
			i++;
		endwhile

	endfor


endfunction

#rep_x2(abs(rep_x2(:)-X(2))>epsilon2)=0;
