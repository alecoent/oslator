function [C,G] =zonotope_decompo(zonotope)
	C = zonotope(:,1);
	G = zonotope(:,2:length(zonotope(1,:)));
endfunction
