function point_image = affine_transfo(point,mode)

	if mode ==0
		point_image = point;
	else
		point_image = [alpha_c(mode,point)*point + beta_c(mode,point)];
	endif
endfunction
