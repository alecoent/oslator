function morceau = split_zonotope(zonotope,chunk)
	[C,G]=zonotope_decompo(zonotope);
	global dim;
#c'est ici pour la dimension --------------|
	morceau = [C + 1/2*G*int2bin(chunk,dim,-1),1/2*G];
endfunction
