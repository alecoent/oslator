function [value] = maximin(x,depth,player,V,U)

	global rep_x1
	global rep_x2
	global numberModes
	global V1
	global V2

	indexA = closest_representative_index(x(1:2),rep_x1,rep_x2);
	indexB = closest_representative_index(x(3:4),rep_x1,rep_x2);
	
	if depth == 0
		value = V(indexA(1),indexA(2),indexB(1),indexB(2),1);

		#action = V(indexA(1),indexA(2),indexB(1),indexB(2),depth,player);
	endif

	if player == 1
		value = 10000;
		for k=1:numberModes
			x = [closest_representative(affine_transfo(x(1:2),V1,k),rep_x1,rep_x2);x(3:4)];
			value = min(value,maximin(x,depth-1,2,V,U));
		endfor
	else 	
		value = -10000;
		for k=1:numberModes
			x = [x(1:2);closest_representative(affine_transfo(x(3:4),V2,k),rep_x1,rep_x2)];
			value = max(value,maximin(x,depth-1,1,V,U));
		endfor
	endif

endfunction
