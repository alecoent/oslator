function rep = closest_representative_index(X,x1,x2)
	global epsilon1
	global epsilon2
#	rep1 = x1(abs(x1(:)-X(1))<=epsilon/sqrt(2));
#	rep2 = x2(abs(x2(:)-X(2))<=epsilon/sqrt(2));

	flag1=1;
	flag2=1;
	i=1;
	while(flag1 && i<=length(x1))
		if abs(x1(i)-X(1))<=epsilon1
			rep1 = i;
			flag1=0;
		endif
		i++;
	endwhile

	i=1;
	while(flag2 && i<=length(x2))
		if abs(x2(i)-X(2))<=epsilon2
			rep2 = i;
			flag2=0;
		endif
		i++;
	endwhile
#	x1(abs(x1(:)-X(1))>epsilon1)=0;
#	x2(abs(x2(:)-X(2))>epsilon2)=0;
#	rep1=find(x1);
#	rep2=find(x2);
	rep = [rep1(1);rep2(1)];
endfunction

#rep_x2(abs(rep_x2(:)-X(2))>epsilon2)=0;
