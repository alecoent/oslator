function zon_image = affine_transfo_zon(zon,mode)
	[C,G] = zonotope_decompo(zon);
	
	if mode ==0
		zon_image = zon;
	else
		zon_image = [alpha_c(mode,zon)*C + beta_c(mode,zon),alpha_c(mode,zon)*G];
	endif
endfunction
