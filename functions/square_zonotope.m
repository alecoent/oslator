function squared_zonotope = square_zonotope(zonotope)
	[C,G] = zonotope_decompo(zonotope);
	n=length(G(:,1));
	m=length(G(1,:));
	#n = 1;
	G_squared = zeros(n,n);
	for i=1:n
		for j=1:m
			G_squared(i,i)=G_squared(i,i) + abs(G(i,j));
		endfor
	endfor
	squared_zonotope=[C,G_squared];
endfunction
