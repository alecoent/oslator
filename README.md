OSLator 1.0
=============

This is OSLator, a tool for controller synthesis for Sampled Switched Systems using Semi-Lagrangian Schemes and One-Sided Lipschitz (OSL) Constants.

### Set up ###

In order to run the examples, all you need is:

* [GNU Octave 4.4](http://www.gnu.org/software/octave/)

### Running an example ###

* Examples of dimension 2 (tag chase and two tank)

Files go by pair: file.m and file_simu.m  
The first file computes a controller, the second simulates it. 

To compute a controller for one of the examples, in a terminal, run the command: "octave file.m"  
file.m computes a controller and saves it in ./controllers/result_file.mat

To run a simulation, run the command: "octave file_simu.m"  
file_simu.m runs a simulation using the controller (./controllers/result_file.mat) computed with file.m  
The simulation length and initial conditions are the first two variables given in the simulation file. 

* Examples of dimension greater than 2 (reaction-diffusion using model reduction)

Files go by three: file.m, file_simu.m and file_simu_HD.m  
The first file computes a controller for the reduced system, the second simulates it on the reduced system, and the third one simulates it on the full system. 

To compute a controller for one of the examples, in a terminal, run the command: "octave file.m"  
file.m computes a controller and saves it in ./controllers/result_file.mat

To run a simulation of the reduced system, run the command: "octave file_simu.m"  
file_simu.m runs a simulation of the reduced system using the controller (./controllers/result_file.mat) computed with file.m  
The simulation length and initial conditions are given in the beginning of the simulation file.  
The control sequence used in the simulation is saved in ./controllers/pattern.mat 

To run a simulation of the full system, run the command: "octave file_simu_HD.m"  
file_simu_HD.m runs a simulation of the full system using the control sequence (./controllers/pattern.mat) computed with file_simu.m  
The simulation length and initial conditions must be the same as in file_simu.m

### Bibliography ###

Information on the methods used in the tool can be found in the following references:

* [A. Le Coënt, L. Fribourg, Guaranteed Control of Sampled Switched Systems using Semi-Lagrangian Schemes and One-Sided Lipschitz Constants](https://arxiv.org/pdf/1903.05882.pdf)

* [A. Le Coënt, L. Fribourg, Guaranteed optimal reachability control of reaction-diffusion equations using one-sided Lipschitz constants and model reduction](https://hal.archives-ouvertes.fr/hal-02193769/document)
