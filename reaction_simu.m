####################################################################
# Simulation file for the (reduced) reaction-diffusion case study available in
# [A. Le Coënt, L. Fribourg, Guaranteed optimal reachability control of reaction-diffusion equations using one-sided Lipschitz constants and model reduction](https://hal.archives-ouvertes.fr/hal-02193769/document)
####################################################################


####################################################################
### FUNCTIONS AND VARIABLES ########################################
####################################################################

addpath('./functions')

 tic 

load result_reaction.mat



global maxLength = 1;
global disc_x = 15;
global disc_y = 5;
global L = 4;
global delta_x = L/(disc_y-1);
global dim = disc_y-2;#disc_y-2;
global epsilon;

T_max = 20;  #max time for the simulation
X00 = 0:L/(disc_y-1):L
X0 = 0.8.*X00/L + 0.1.*(1-X00./L) #initial condition
X0 = X0'
Y0 = X0(2:end-1)

global obj = 0;

global theta = 0.3;#1/3;
global L = 4;
#global delta_x = 1;
global T = 4;
global tau = 0.1;

global sigma = 0.5;

#global uu = [0,0,0,0.5,0.5,0.5,1,1,1;0,0.5,1,0,0.5,1,0,0.5,1];

global uu = [0,0.2,0.4,0.6,0.8,1.0;0,0.2,0.4,0.6,0.8,1.0];

global numberModes = length(uu);

xmin = 0.0;
xmax= 1.0;

xmin_safe = 0.0;
xmax_safe = 1.0;

global ff = @(y) y.*(1-y).*(y-theta);


function y_dt = f_dt(y,mode)
	global delta_x
	global disc_y
	global uu
	global ff
	global sigma

	y = [uu(1,mode);y(2:end-1);uu(2,mode)];

	y_dt = zeros(disc_y,1);

	y_dt(1) = 0;#uu(1,mode);
	y_dt(disc_y) = 0;#uu(2,mode);
	y_dt(2:(disc_y-1)) = sigma/(delta_x^2)*(y(3:disc_y)-2*y(2:(disc_y-1))+y(1:disc_y-2)) + ff(y(2:(disc_y-1)));
endfunction


function y_dt = f_dt_inside(y,mode)
	global delta_x
	global disc_y
	global uu
	global ff
	global sigma

	y = [uu(1,mode);y;uu(2,mode)];

	y_dt = zeros(disc_y,1);

	y_dt(1) = 0;#uu(1,mode);
	y_dt(disc_y) = 0;#uu(2,mode);
	y_dt(2:(disc_y-1)) = sigma/(delta_x^2)*(y(3:disc_y)-2*y(2:(disc_y-1))+y(1:disc_y-2)) + ff(y(2:(disc_y-1)));
endfunction

global fT = @(t,T,mode) [f_dt(T,mode)];
global fT_inside = @(t,T,mode) [f_dt_inside(T,mode)];

function A = alpha_c(mode,zonotope)
	global dim
	A = eye(dim);	
endfunction 

function B = beta_c(mode,zonotope)
	global tau
	global fT_inside
	DF = fT_inside(0,zonotope(:,1),mode);
	B = tau*DF(2:end-1);
endfunction 


function value = cost(state)
	global theta
	value = norm(state-theta);
endfunction

function [t,y] = post_y(y0,mode)
	global h
	global fT
	global tau
	global uu

	y0 = [uu(1,mode);y0(2:end-1);uu(2,mode)];
	vopt = odeset ("RelTol", 1e-10, "AbsTol", 1e-10,"NormControl", "on");
		[t,y] = ode45(@(t,x) fT(t,x,mode),[0,tau],y0,vopt);
#		y(:,1) = ones(1,length(t))*uu(1,mode);
#		y(:,end) = ones(1,length(t))*uu(2,mode);

endfunction

####################################################################
### TILING #########################################################
####################################################################

S_safe = [ones(dim,1)*(xmin_safe+xmax_safe)/2,eye(dim)*(xmax_safe-xmin_safe)/2];

x = linspace(xmin_safe,xmax_safe,disc_x);

epsilon = 1/2*norm([(xmax_safe-xmin_safe)/(disc_x-1)]);


rep_x = (x(2:end)+x(1:end-1))/2;
[rep_X1,rep_X2] = meshgrid(rep_x,rep_x);


nn = dim;
xx = (0:(disc_x-1)^dim-1)';
MM = zeros((disc_x-1)^dim,dim);

for k = 1:nn
	MM(:,k) = mod(xx,disc_x-1);
	xx = (xx-MM(:,k))/(disc_x-1);
endfor

multi_index = (MM+1)';

#keyboard
X = 0.5*ones(dim,1);
rep = closest_representative(X,rep_x);
X = rep;

mem = 1000;
mem_index = 0;




#####################################################################
#### SIMULATION #####################################################
#####################################################################


toc

#save result_EDP.mat U



#keyboard


y = [X0];
t = [0];
m = [0];

Y = [Y0];
T = [0];
M = [0];

for i = 1:T_max
i
	pattern = [];

	index_pat = closest_representative_index(y(2:end-1,end),rep_x);
	for k = 2:maxLength+1

		U_temp = U(:);
		index_temp0 = 1;
		for k4 = 1:dim
#keyboard
			index_temp0 = index_temp0+((disc_x-1)^(k4-1))*(index_pat(k4)-1);
#keyboard
		endfor
		index_temp = index_temp0 + (disc_x-1)^(dim)*(k-1);
		mode_temp = U_temp(index_temp);

    		pattern = [pattern,mode_temp];

	endfor

pattern

    	for j = 1:length(pattern)
#keyboard

		[tt,yy] = post_y(y(:,end),pattern(j));

		y = [y,yy'];
		t = [t,t(end)+tt'];
		m = [m,pattern(j)*ones(1,length(tt'))];

		[YY] = affine_transfo(Y(:,end),pattern(j));
		Y = [Y,YY];
		T = [T,T(end)+tau];
		M = [M,pattern(j)];

#keyboard
    	endfor
#keyboard
endfor

save pattern.mat M

plot_Y = [[0.1,uu(1,M(2:end))];Y;[0.8,uu(2,M(2:end))]];
plot_X = 0:L/(disc_y-1):L

plot_T = (0:length(plot_Y))*0.1
plot_YY = plot_Y(:,1)'


figure(2)
plot3(zeros(size(plot_YY)),plot_X,plot_YY','color','black')
hold on
for i = 1:length(plot_Y)
	plot_YY = plot_Y(:,i);
 	plot3(ones(size(plot_YY))*plot_T(i),plot_X,plot_YY','color','black')
endfor

lab1=xlabel("t");
lab2=ylabel("x");
lab3=zlabel("y");
set(lab1,'fontsize',16,'linewidth',5);
set(lab2,'fontsize',16,'linewidth',5);
h=get(gcf,'currentaxes');
set(h,'fontsize',12,'linewidth',1)

err_int = 0;

for i = 1:disc_y-1
	err_int = err_int + ((plot_YY(i)-0.3)^2+(plot_YY(i+1)-0.3)^2)/2*delta_x;
endfor

err_euc = norm(plot_YY-theta*ones(disc_y,1))

keyboard 


