####################################################################
# Controller synthesis file for the tag-chase case study available in
# [A. Le Coënt, L. Fribourg, Guaranteed Control of Sampled Switched Systems using Semi-Lagrangian Schemes and One-Sided Lipschitz Constants](https://arxiv.org/pdf/1903.05882.pdf)
####################################################################


####################################################################
### FUNCTIONS AND VARIABLES ########################################
####################################################################

addpath('./functions')

debug_on_error(1)
debug_on_interrupt(1)

#keyboard
tic 
#dbstop in closest_representative at 5 if error

global dim = 2;
global tau = 0.2;
global numberModes = 6;
global numberModesA = numberModes;
global numberModesB = numberModes;
global maxLength = 1;
global disc_x1 = 10;
global disc_x2 = 10;
global epsilon1;
global epsilon2;
global obj = [1.0;0.0];

x1min = -2;
x1max= 2;
x2min = -2;
x2max = 2;

x1min_safe = -2;
x1max_safe = 2;
x2min_safe = -2;
x2max_safe = 2;

global V1 = 1.1;
global V2 = 1.0;

global theta = linspace(0,2*pi,numberModes+1)'; 
#modes_A = t(1:end-1);
#modes_B = t(1:end-1);


#circsx = delta0.*cos(t) + X0(1); 
#circsy = delta0.*sin(t) + X0(2); 
#plot(circsx,circsy,'color','black','linewidth',2); 


global fT = @(x,T,modeV,modeT) [modeV*sin(theta(modeT));modeV*cos(theta(modeT))];

function A = alpha_c(modeV,modeT)
	A = eye(2);
endfunction 

function B = beta_c(modeV,modeT)
	global tau;
	global theta;
	B = tau*[modeV*sin(theta(modeT));modeV*cos(theta(modeT))];

endfunction 

function point_image = affine_transfo(point,modeV,modeT)
	if modeT ==0
		point_image = point;
	else
		point_image = [alpha_c(modeV,modeT)*point + beta_c(modeV,modeT)];
	endif
endfunction

function zon_image = affine_transfo_zon(zon,modeV,modeT)
	[C,G] = zonotope_decompo(zon);
	
	if modeT ==0
		zon_image = zon;
	else
		zon_image = [alpha_c(modeV,modeT)*C + beta_c(modeV,modeT),alpha_c(modeV,modeT)*G];
	endif
endfunction

function value = cost(state)
	value = sqrt((state(1)-state(3))^2+(state(2)-state(4))^2);
endfunction

function [t,y] = post_y(y0,modeV,modeT)
	global h;
	global fT;
	global tau;
	vopt = odeset ("RelTol", 1e-10, "AbsTol", 1e-10,"NormControl", "on");
		[t,y] = ode45(@(t,x) fT(t,x,modeV,modeT),[0,tau],y0,vopt);
endfunction

####################################################################
### TILING #########################################################
####################################################################


S_safe = [(x1min_safe+x1max_safe)/2,(x1max_safe-x1min_safe)/2,0,0,0;
	  (x2min_safe+x2max_safe)/2,0,(x2max_safe-x2min_safe)/2,0,0;
	  (x1min_safe+x1max_safe)/2,0,0,(x1max_safe-x1min_safe)/2,0;
	  (x2min_safe+x2max_safe)/2,0,0,0,(x2max_safe-x2min_safe)/2];

S_safe1 = [(x1min_safe+x1max_safe)/2,(x1max_safe-x1min_safe)/2,0;
	  (x2min_safe+x2max_safe)/2,0,(x2max_safe-x2min_safe)/2];

S_safe2 = [(x1min_safe+x1max_safe)/2,(x1max_safe-x1min_safe)/2,0;
	  (x2min_safe+x2max_safe)/2,0,(x2max_safe-x2min_safe)/2];


x1 = linspace(x1min_safe,x1max_safe,disc_x1);
x2 = linspace(x2min_safe,x2max_safe,disc_x2);
#epsilon = 1/2*norm([(x1max_safe-x1min_safe)/(disc_x1-1);(x2max_safe-x2min_safe)/(disc_x2-1)]);
epsilon1 = 1/2*abs([(x1max_safe-x1min_safe)/(disc_x1-1)]);
epsilon2 = 1/2*abs([(x2max_safe-x2min_safe)/(disc_x2-1)]);
[X1,X2] = meshgrid(x1,x2);

global rep_x1 = (x1(2:end)+x1(1:end-1))/2;
global rep_x2 = (x2(2:end)+x2(1:end-1))/2;
[rep_X1,rep_X2] = meshgrid(rep_x1,rep_x2);

V = zeros([size(rep_X1),size(rep_X1),maxLength+1]);
U = zeros([size(rep_X1),size(rep_X1),maxLength+1,2]);



for i = 1:disc_x1-1
	for j = 1:disc_x2-1
		for k = 1:disc_x1-1
			for l = 1:disc_x2-1
				V(i,j,k,l,1) = cost([rep_x1(i);rep_x2(j);rep_x1(k);rep_x2(l)]);
			endfor
		endfor
	endfor
endfor

X = [1.3;0.2;-1.2;-0.5];
repA = closest_representative(X(1:2),rep_x1,rep_x2);
repB = closest_representative(X(3:4),rep_x1,rep_x2);
#X = rep;

#keyboard



mem = 1000;
mem_index = 0;

for k1 = 1:disc_x1-1
k1
	for k2 = 1:disc_x2-1
#k2
		for k3 = 1:disc_x1-1
			for k4 = 1:disc_x2-1

				X0 = [rep_x1(k1);rep_x2(k2);rep_x1(k3);rep_x2(k4)];
				X = X0;
				zon1 = [X(1),epsilon1,0;X(2),0,epsilon2];
				zon2 = [X(3),epsilon1,0;X(4),0,epsilon2];

				for i = 1:maxLength

					memA = 10000;
					memB = -1;
					memAmem = 100000;
					memBmem = -1;
					#mem_index = 0;
					mem_nextA = X0(1:2);
					mem_nextB = X0(3:4);
		#i
		#keyboard

#					value = maximin(X,2,1,V,U)
#keyboard
				
#					while((memAmem != memA) || (memBmem !=memB))
#						memAmem = memA;
#						memBmem = memB;

					payoff_matrix1 = zeros(numberModes,numberModes);
					payoff_matrix2 = zeros(numberModes,numberModes);

					for j2 = 1:numberModesB
						for j1 = 1:numberModesA

							flag = 0;
							next_X = [affine_transfo(X(1:2),V1,j1);affine_transfo(X(3:4),V2,j2)];
							next_zon1 = affine_transfo_zon(zon1,V1,j1);
							next_zon2 = affine_transfo_zon(zon2,V2,j2);
#	keyboard
							if is_in_zonotope(next_X,S_safe) && inclusion_zonotope(next_zon1,S_safe1) && inclusion_zonotope(next_zon2,S_safe2)
#keyboard

#next_X
								#nextA = closest_representative(next_X(1:2),rep_x1,rep_x2);
								#nextB = closest_representative(next_X(3:4),rep_x1,rep_x2);
								next = next_X;#[nextA;nextB];
#keyboard
# next_X =[-1.71429;1.42857;-1.71429;-0.71429]


#								for k5 = 1:i
#									next_X = [affine_transfo(nextA,V1,U(k1,k2,k3,k4,k5,1));affine_transfo(nextB,V2,U(k1,k2,k3,k4,k5,2))];
#			#keyboard
#									if is_in_zonotope(next_X,S_safe)
#										next = [closest_representative(next_X(1:2),rep_x1,rep_x2);closest_representative(next_X(3:4),rep_x1,rep_x2)];
#									else
#										flag = 1
#										break
#									endif
#								endfor
##keyboard
#								if flag == 1
#									break
#								endif

								value = cost(next);
#keyboard

								payoff_matrix1(j1,j2) = value;
								payoff_matrix2(j1,j2) = value;
								
#keyboard
#									if memA>value
#										memA = value;
#										#mem_index = j;
#										mem_nextA = next(1:2);
#										U(k1,k2,k3,k4,i+1,1) = j1;
#										V(k1,k2,k3,k4,i+1) = memA;
#									endif

#									if memB<=value
#										memB = value;
#										#mem_index = j;
#										mem_nextB = next(3:4);
#										U(k1,k2,k3,k4,i+1,2) = j2;
#										V(k1,k2,k3,k4,i+1) = memB;
#									endif

			#				else 	
			#					value = cost(next_X);
			#					mem = 1000;
			#					#mem_index = 0;
			#					U(k1,k2,i+1) = 0;
			#					V(k1,k2,i+1) = mem;

							else 
								payoff_matrix1(j1,j2) = -1;
								payoff_matrix2(j1,j2) = -1;
							endif
							
						endfor

						#X = mem_next;

					#	[next_X,arg_min] = min(cost(affine_transfo(X,1:4)),[],2)

					endfor

				### action_A
					for j1 = 1:numberModes
						toto = payoff_matrix1(j1,:);
						[value,action] = max(toto(toto>=0));

						if memA >= value
							memA = value;
							U(k1,k2,k3,k4,i+1,1) = j1;
							V(k1,k2,k3,k4,i+1) = memA;
						endif
					endfor

					for j2 = 1:numberModes
						toto = payoff_matrix2(:,j2);
						[value,action] = min(toto(toto>=0));

						if memB < value
							memB = value;
							U(k1,k2,k3,k4,i+1,2) = j2;
							V(k1,k2,k3,k4,i+1) = memB;
						endif
					endfor
#keyboard
#				if U(k1,k2,k3,k4,i+1,2) == 0
#					keyboard
#				endif

				endfor
			endfor
		endfor
	endfor
endfor



save ./controllers/result_tag_chase.mat U

toc

####################################################################
### SIMULATION #####################################################
####################################################################



keyboard
T_max = 11;
X0 = [1.3;0.2;0.5;-0.5];

#X0 = [closest_representative(X0(1:2),rep_x1,rep_x2);closest_representative(X0(3:4),rep_x1,rep_x2)];

y1 = [X0(1:2)];
y2 = [X0(3:4)];
t1 = [0];
t2 = [0];

Y1 = [X0(1:2)];
T1 = [0];
Y2 = [X0(3:4)];
T2 = [0];

keyboard

for i = 1:T_max
i
	pattern1 = [];
	pattern2 = [];
#keyboard
	index_pat = [closest_representative_index(y1(:,end),rep_x1,rep_x2);closest_representative_index(y2(:,end),rep_x1,rep_x2)];
	for k = 1:maxLength
    		pattern1 = [pattern1,U(index_pat(1),index_pat(2),index_pat(3),index_pat(4),k+1,1)];
    		pattern2 = [pattern2,U(index_pat(1),index_pat(2),index_pat(3),index_pat(4),k+1,2)];
	endfor

#	next_pat = y(:,end);

#	

#	for k = 1:maxLength

#		index_pat = closest_representative_index(next_pat,rep_x1,rep_x2);
#		next_pat = closest_representative(next_pat,rep_x1,rep_x2);

#    		pattern = [U(index_pat(1),index_pat(2),maxLength-k+2),pattern];
#		next_pat = closest_representative(y(:,end),rep_x1,rep_x2);
#		for kk = 1:k
#			next_pat = closest_representative(affine_transfo(next_pat,pattern(kk)),rep_x1,rep_x2);
#		endfor

#		#index_pat = closest_representative_index(next_pat,rep_x1,rep_x2);
#		
#	endfor

#pattern
#keyboard
    	for j = 1:length(pattern1)
		[tt1,yy1] = post_y(y1(:,end),V1,pattern1(j));
#keyboard
		[tt2,yy2] = post_y(y2(:,end),V2,pattern2(j));
		y1 = [y1,yy1'];
		t1 = [t1,t1(end)+tt1'];
		y2 = [y2,yy2'];
		t2 = [t2,t2(end)+tt2'];

		[YY1] = affine_transfo(Y1(:,end),V1,pattern1(j));
		[YY2] = affine_transfo(Y2(:,end),V2,pattern2(j));
		Y1 = [Y1,YY1];
		Y2 = [Y2,YY2];
		T1 = [T1,T1(end)+tau];
		T2 = [T2,T2(end)+tau];

#keyboard
    	endfor
#keyboard
endfor

plot(t1,y1)
hold on
plot(t2,y2)
plot(T1,Y1)
plot(T2,Y2)

figure(2)
plot(Y1(1,:),Y1(2,:),'-ob') 
#plot(Y1(1,:),Y1(2,:),'color','blue')
hold on
plot(Y2(1,:),Y2(2,:),'-or') 
leg=legend('pursuer','evader','fontsize', 16,'linewidth',2);
#set(leg, 'fontsize', 16);
#et(leg,'linewidth',2);
#set(leg,'location','southeast');
lab1=xlabel("x_1");
lab2=ylabel("x_2");
set(lab1,'fontsize',16,'linewidth',5);
set(lab2,'fontsize',16,'linewidth',5);
h=get(gcf,'currentaxes');
set(h,'fontsize',12,'linewidth',2)
print -dpdf simu_two_tank.pdf


#plot(y2(1,:),y2(2,:),'color','red')
#plot(obj(1),obj(2),'or')


keyboard

